FROM tomcat:9-jdk11-openjdk-slim
ADD /maven/${docker.filename}-${env.profile}.${docker.packaging} /usr/local/tomcat/webapps/ROOT.war
#ADD /maven/truststore.jks /usr/local/app/truststore.jks
#ADD /maven/known_hosts /root/.ssh/known_hosts
ENV CATALINA_OPTS="$CATALINA_OPTS ${docker.memory.java.heap}"
WORKDIR /usr/local/sandbox
EXPOSE 8080
#ENTRYPOINT keytool -importkeystore -srckeystore truststore.jks -destkeystore /usr/local/openjdk-11/lib/security/cacerts -srcstorepass changeit -deststorepass changeit \ && catalina.sh run
ENTRYPOINT catalina.sh run