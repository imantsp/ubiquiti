package io.restudie.model.enums;

import java.util.Arrays;

public enum DeviceType {
    GATEWAY(1L, "Gateway", 1),
    SWITCH(2L, "Switch", 2),
    ACCESSPOINT(3L, "Access Point", 3);

    private Long id;
    private String description;
    private Integer sortOrder;

    DeviceType(Long id, String description, Integer sortOrder) {
        this.id = id;
        this.description = description;
        this.sortOrder = sortOrder;
    }


    public String getDescription() {
        return description;
    }

    public Long getId() {
        return id;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public static DeviceType getByType(Long id){
        return Arrays.stream(DeviceType.values())
                .filter(t -> t.getId().equals(id))
                .findFirst()
                .orElse(null);
    }
}