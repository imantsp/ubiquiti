package io.restudie.model;

import io.restudie.model.enums.DeviceType;
import lombok.Data;

@Data
public class Device {

    private DeviceType type;
    private String macAddress;
    private String uplinkMacAddress;

    public Device(){
    }

    public Device(String macAddress){
         this.macAddress = macAddress;
    }

    public Device(DeviceType type, String macAddress, String uplinkMacAddress){
        this.type = type;
        this.macAddress = macAddress;
        this.uplinkMacAddress = uplinkMacAddress;
    }
}
