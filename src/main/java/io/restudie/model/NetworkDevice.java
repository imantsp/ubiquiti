package io.restudie.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
public class NetworkDevice extends Device {
    private List<NetworkDevice> linkedDevices;

    public NetworkDevice(){}

    public NetworkDevice(String macAddress){
        setMacAddress(macAddress);
    }

    public NetworkDevice(Device device){
        super(device.getType(), device.getMacAddress(), device.getUplinkMacAddress());
    }

    public void add(NetworkDevice networkDevice){
        this.linkedDevices = Objects.requireNonNullElse(this.linkedDevices, new ArrayList<>());
        this.linkedDevices.add(networkDevice);
    }
}
