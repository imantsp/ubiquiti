package io.restudie.model;

import java.util.Collection;
import java.util.HashMap;

public class NetworkTopology extends HashMap<String, NetworkDevice> {

    public Collection<NetworkDevice> getNetworkDevicesTree(){
        return values();
    }
}
