package io.restudie.exception;

import io.restudie.validation.Message;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DeviceApiException extends Exception {
    private final List<Message> errorMessages = new ArrayList<>();

    public DeviceApiException(List<Message> errorMessages) {
        this.errorMessages.addAll(errorMessages);
    }

    public DeviceApiException(Message errorMessage) {
         this.errorMessages.add(errorMessage);
    }

    public List<Message> getErrorMessages() {
        return errorMessages;
    }

    public DeviceApiException() {
        super();
    }

    public DeviceApiException(List<Message> errorMessages, String technicalMessage, Throwable cause) {
        super(technicalMessage, cause);
        this.errorMessages.addAll(errorMessages);
    }

    public void addErrorMessage(Message errorMessage) {
        errorMessages.add(errorMessage);
    }

    public String buildErrorMessage() {
        String errors = "";

        if (CollectionUtils.isNotEmpty(getErrorMessages())) {
            errors = this.errorMessages
                    .stream()
                    .map(error -> error.getCode() + " - " + error.getText())
                    .collect(Collectors.joining("\n"));
        } else if (getMessage() != null) errors = getMessage();

        return errors;
    }
}
