package io.restudie.exception;

import io.restudie.controller.response.BasicResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice()
@Log4j2
public class DeviceApiExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {DeviceApiException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ResponseEntity<Object> handleException(DeviceApiException exception, WebRequest request) {
        log.error(exception.buildErrorMessage(), exception);
        BasicResponse responseBody = new BasicResponse(exception.getErrorMessages(), exception.getMessage());
        return handleExceptionInternal(exception, responseBody, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value = {RuntimeException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    protected ResponseEntity<Object> handleException(RuntimeException exception, WebRequest request) {
        log.error(exception.getMessage(), exception);
        BasicResponse responseBody = new BasicResponse(null, exception.getMessage());
        return handleExceptionInternal(exception, responseBody, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    @ExceptionHandler(value ={MethodArgumentTypeMismatchException.class})
    protected ResponseEntity<Object> handleMethodArgumentTypeMismatchException(RuntimeException exception, WebRequest request) {
        log.error(exception.getMessage(), exception);
        BasicResponse responseBody = new BasicResponse(null, exception.getMessage());
        return handleExceptionInternal(exception, responseBody, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException exception, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.error(exception.getMessage(), exception);
        BasicResponse responseBody = new BasicResponse(null, exception.getMessage());
        return handleExceptionInternal(exception, responseBody, new HttpHeaders(), status, request);
    }

}
