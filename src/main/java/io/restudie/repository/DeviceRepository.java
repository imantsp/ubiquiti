package io.restudie.repository;

import io.restudie.repository.entity.DeviceData;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DeviceRepository extends CrudRepository<DeviceData, Long> {
    List<DeviceData> findDeviceDataByMacAddressContainingIgnoreCase(String macAddress);
}
