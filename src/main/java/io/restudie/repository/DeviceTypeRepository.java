package io.restudie.repository;

import io.restudie.repository.entity.DeviceTypeData;
import org.springframework.data.repository.CrudRepository;

public interface DeviceTypeRepository extends CrudRepository<DeviceTypeData, Long> {
}
