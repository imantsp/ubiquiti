package io.restudie.repository.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "DEVICES", schema = "PUBLIC")
@Data
public class DeviceData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", updatable = false, nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "DEVICE_TYPE_ID")
    private DeviceTypeData type;

    @Column(name = "MAC_ADDRESS", unique=true)
    private String macAddress;

    @ManyToOne(fetch = FetchType.LAZY)
    private DeviceData uplinkDevice;

    @OneToMany(mappedBy = "uplinkDevice")
    private List<DeviceData> childrenDevices;

}
