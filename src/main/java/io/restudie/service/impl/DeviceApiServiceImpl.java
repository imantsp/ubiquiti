package io.restudie.service.impl;


import io.restudie.exception.DeviceApiException;
import io.restudie.model.Device;
import io.restudie.model.NetworkDevice;
import io.restudie.model.NetworkTopology;
import io.restudie.repository.DeviceRepository;
import io.restudie.repository.DeviceTypeRepository;
import io.restudie.repository.entity.DeviceData;
import io.restudie.repository.entity.DeviceTypeData;
import io.restudie.service.DeviceApiService;
import io.restudie.service.mapper.DeviceDataMapper;
import io.restudie.validation.validator.DeviceDataValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Log4j2
@Service
public class DeviceApiServiceImpl implements DeviceApiService {

    private final DeviceRepository deviceRepository;
    private final DeviceTypeRepository deviceTypeRepository;
    private final DeviceDataMapper<Device, DeviceData> deviceDataMapper;

    public DeviceApiServiceImpl(DeviceRepository deviceRepository,
                                DeviceTypeRepository deviceTypeRepository,
                                DeviceDataMapper<Device, DeviceData> deviceDataMapper) {
        this.deviceRepository = Objects.requireNonNull(deviceRepository);
        this.deviceTypeRepository = Objects.requireNonNull(deviceTypeRepository);
        this.deviceDataMapper = Objects.requireNonNull(deviceDataMapper);
    }

    /**
     * Validate and save Device in database
     *
     * @param device device to register
     * @return registred device
     * @throws DeviceApiException
     */
    @Override
    public Device registerDevice(Device device) throws DeviceApiException {
        DeviceData deviceData = null;
        DeviceData uplinkDevice = null;

        List<DeviceData> deviceDataList = deviceRepository.findDeviceDataByMacAddressContainingIgnoreCase(device.getMacAddress());
        List<DeviceData> uplinkDeviceDataList = Objects.nonNull(device.getUplinkMacAddress()) ?
                deviceRepository.findDeviceDataByMacAddressContainingIgnoreCase(device.getUplinkMacAddress()) : new ArrayList<>();
        Optional<DeviceTypeData> typeFound = deviceTypeRepository.findById(device.getType().getId());

        //Validations
        DeviceDataValidator.validateMacAddressExistInDatabase(deviceDataList);
        DeviceDataValidator.validateUplinkMacAddressNotExistInDatabase(device.getUplinkMacAddress(), uplinkDeviceDataList);

        uplinkDevice = uplinkDeviceDataList.stream().findFirst().orElse(null);

        deviceData = deviceDataMapper.toEntity(device);
        deviceData.setType(typeFound.orElse(null));
        deviceData.setUplinkDevice(uplinkDevice);

        deviceRepository.save(deviceData);
        return deviceDataMapper.toDto(deviceData);
    }

    /**
     * Retrieving all registered devices, sorted by device type
     *
     * @return
     * @throws DeviceApiException
     */
    @Override
    public List<Device> getDeviceList(Boolean isSorted) throws DeviceApiException {
        Iterable<DeviceData> deviceDataIterable = deviceRepository.findAll();
        List<Device> deviceList = new ArrayList<>();
        deviceDataIterable.forEach(data -> deviceList.add(deviceDataMapper.toDto(data)));

        /*sort*/
        if (Boolean.TRUE.equals(isSorted)) deviceList.sort(new DeviceComparator());

        return deviceList;
    }

    /**
     * Retrieving network deployment device by MAC address
     *
     * @param macAddress
     * @return
     * @throws DeviceApiException
     */
    @Override
    public Device getDeviceByMacAddress(String macAddress) throws DeviceApiException {
        Device device = null;

        DeviceDataValidator.validateMacAddressProvided(macAddress);

        List<DeviceData> deviceDataList = deviceRepository.findDeviceDataByMacAddressContainingIgnoreCase(macAddress);
        if (CollectionUtils.isNotEmpty(deviceDataList)) device = deviceDataMapper.toDto(deviceDataList.get(0));

        DeviceDataValidator.validateDeviceExist(device);

        return device;
    }

    /**
     * Retrieving all registered network device topology
     *
     * @return
     * @throws DeviceApiException
     */
    @Override
    public NetworkTopology getNetworkTopology() throws DeviceApiException {
        Map<String, NetworkDevice> devicesMap = getDeviceList(false)
                .stream()
                .collect(Collectors.toMap(Device::getMacAddress, NetworkDevice::new));

        return buildNetworkTopology(devicesMap);
    }

    /**
     * Retrieving network device topology starting from a specific device mac address
     *
     * @param macAddress
     * @return
     * @throws DeviceApiException
     */
    @Override
    public NetworkTopology getNetworkTopologyByMacAddress(String macAddress) throws DeviceApiException {
        DeviceDataValidator.validateMacAddressProvided(macAddress);
        Device device = getDeviceByMacAddress(macAddress);
        DeviceDataValidator.validateDeviceExist(device);

        NetworkTopology networkTopology = getNetworkTopology();
        return searchInTopology(networkTopology, macAddress);
    }

    /**
     * used to sort by device type
     */
    private class DeviceComparator implements Comparator<Device> {
        @Override
        public int compare(Device device1, Device device2) {
            return device1.getType().getSortOrder().compareTo(device2.getType().getSortOrder());
        }
    }

    private NetworkTopology buildNetworkTopology(Map<String, NetworkDevice> devicesMap) {
        NetworkTopology topology = new NetworkTopology();

        for (NetworkDevice current : devicesMap.values()) {
            if (current.getUplinkMacAddress() != null) {
                NetworkDevice uplinkDevice = devicesMap.get(current.getUplinkMacAddress());
                uplinkDevice.add(current);
            } else {
                topology.put(current.getMacAddress(), current);
            }
        }

        return topology;
    }

    private NetworkTopology searchInTopology(Map<String, NetworkDevice> devicesMap, String macAddress) {
        NetworkTopology filteredTopology = new NetworkTopology();

        for (NetworkDevice current : devicesMap.values()) {
            if (current.getMacAddress().equalsIgnoreCase(macAddress)) {
                filteredTopology.put(current.getMacAddress(), devicesMap.get(current.getMacAddress()));
                break;
            } else if (CollectionUtils.isNotEmpty(current.getLinkedDevices())) {
                Map<String, NetworkDevice> linkedDevicesMap = current.getLinkedDevices()
                        .stream()
                        .collect(Collectors.toMap(NetworkDevice::getMacAddress, Function.identity()));

                filteredTopology = searchInTopology(linkedDevicesMap, macAddress);

                //stop searching if topology is found
                if (MapUtils.isNotEmpty(filteredTopology)) {
                    return filteredTopology;
                }
            }
        }

        return filteredTopology;
    }

}
