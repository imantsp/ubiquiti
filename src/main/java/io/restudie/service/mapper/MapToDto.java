package io.restudie.service.mapper;

public interface MapToDto<D,E> {
    D toDto(E e);
}
