package io.restudie.service.mapper;

public interface NetworkDeviceMapper<D,E> extends MapToEntity<D,E>, MapToDto<D,E>{
}
