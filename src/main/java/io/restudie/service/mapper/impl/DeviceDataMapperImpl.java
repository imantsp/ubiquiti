package io.restudie.service.mapper.impl;

import io.restudie.model.Device;
import io.restudie.model.enums.DeviceType;
import io.restudie.repository.entity.DeviceData;
import io.restudie.service.mapper.DeviceDataMapper;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class DeviceDataMapperImpl implements DeviceDataMapper<Device, DeviceData> {

    @Override
    public Device toDto(DeviceData deviceData) {
        if (Objects.isNull(deviceData)) return null;

        Device device = new Device();
        device.setType(DeviceType.getByType(deviceData.getType().getId()));
        device.setMacAddress(deviceData.getMacAddress());

        if (Objects.nonNull(deviceData.getUplinkDevice())) {
            device.setUplinkMacAddress(deviceData.getUplinkDevice().getMacAddress());
        }
        return device;
    }

    @Override
    public DeviceData toEntity(Device device) {
        if (Objects.isNull(device)) return null;

        DeviceData deviceData = new DeviceData();
        deviceData.setMacAddress(device.getMacAddress());

        return deviceData;
    }
}
