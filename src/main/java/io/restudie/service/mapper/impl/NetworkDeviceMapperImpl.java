package io.restudie.service.mapper.impl;

import io.restudie.exception.DeviceApiException;
import io.restudie.validation.ErrorMessage;
import io.restudie.validation.Errors;
import io.restudie.model.NetworkDevice;
import io.restudie.model.enums.DeviceType;
import io.restudie.repository.entity.DeviceData;
import io.restudie.service.mapper.NetworkDeviceMapper;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;

@Component
public class NetworkDeviceMapperImpl implements NetworkDeviceMapper<NetworkDevice, DeviceData> {

    @Override
    public NetworkDevice toDto(DeviceData deviceData) {
        NetworkDevice networkDevice = new NetworkDevice();
        networkDevice.setType(DeviceType.getByType(deviceData.getType().getId()));
        networkDevice.setMacAddress(deviceData.getMacAddress());

        if (CollectionUtils.isNotEmpty(deviceData.getChildrenDevices())) {
            networkDevice.setUplinkMacAddress(deviceData.getUplinkDevice().getMacAddress());
        }
        return networkDevice;
    }

    @Override
    public DeviceData toEntity(NetworkDevice networkDevice) throws DeviceApiException {
        throw new DeviceApiException(new ErrorMessage(Errors.NOT_IMPLEMENTED));
    }
}
