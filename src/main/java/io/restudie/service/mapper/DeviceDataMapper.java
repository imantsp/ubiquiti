package io.restudie.service.mapper;

public interface DeviceDataMapper <D,E> extends MapToEntity<D,E>, MapToDto<D,E>{
}
