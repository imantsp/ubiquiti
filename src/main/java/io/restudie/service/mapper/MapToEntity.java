package io.restudie.service.mapper;

import io.restudie.exception.DeviceApiException;

public interface MapToEntity<D,E> {
    E toEntity(D d) throws DeviceApiException;
}
