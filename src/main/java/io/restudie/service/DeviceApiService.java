package io.restudie.service;

import io.restudie.exception.DeviceApiException;
import io.restudie.model.Device;
import io.restudie.model.NetworkTopology;

import java.util.List;

public interface DeviceApiService {

    Device registerDevice(Device request) throws DeviceApiException;
    List<Device> getDeviceList(Boolean isSorted) throws DeviceApiException;
    Device getDeviceByMacAddress(String macAddress) throws DeviceApiException;
    NetworkTopology getNetworkTopology() throws DeviceApiException;
    NetworkTopology getNetworkTopologyByMacAddress(String macAddress) throws DeviceApiException;

}