package io.restudie.validation;

public interface Message {
    public Integer getCode();
    public String getText();
}
