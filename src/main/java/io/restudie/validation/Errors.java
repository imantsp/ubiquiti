package io.restudie.validation;

import java.util.Arrays;
import java.util.Optional;

public enum Errors{
    DEFAULT(-1000, "Default message!"),
    NOT_IMPLEMENTED(-999, "Not implemented!"),
    NOT_HANDLED_ERROR(-998, "Not handled error!"),
    DEVICE_NOT_FOUND(-1, "Device not found!"),
    MAC_ADDRESS_NOT_PROVIDED(-2, "MAC address not provided!"),
    UPLINK_MAC_ADDRESS_NOT_EXIST(-3, "Uplink MAC address not exist!"),
    DEVICE_TYPE_NOT_PROVIDED(-4, "Device type not provided!"),
    MAC_ADDRESS_ALREADY_REGISTERED(-5, "Device with provided MAC address is already registered!");

    private final Integer code;
    private final String text;

    Errors(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    public Integer getCode() {
        return code;
    }

    public String getText() {
        return text;
    }

    public static Errors findByCode(Integer code) {
        Optional<Errors> errorMessage = Arrays.stream(Errors.values())
                .filter(c -> c.getCode().equals(code)).findAny();
        return errorMessage.orElse(DEFAULT);
    }
}

