package io.restudie.validation.validator;

import io.restudie.exception.DeviceApiException;
import io.restudie.model.Device;
import io.restudie.repository.entity.DeviceData;
import io.restudie.validation.ErrorMessage;
import io.restudie.validation.Errors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Objects;

public class DeviceDataValidator {

    private DeviceDataValidator() {
        throw new IllegalStateException();
    }

    public static void validateMacAddressProvided(String macAddress) throws DeviceApiException {
        if (StringUtils.isEmpty(macAddress))
            throw new DeviceApiException(new ErrorMessage(Errors.MAC_ADDRESS_NOT_PROVIDED));
    }

    public static void validateMacAddressProvided(Device device) throws DeviceApiException {
        if (Objects.isNull(device) || StringUtils.isEmpty(device.getMacAddress()))
            throw new DeviceApiException(new ErrorMessage(Errors.MAC_ADDRESS_NOT_PROVIDED));
    }

    public static void validateDeviceExist(Device device) throws DeviceApiException {
        if (Objects.isNull(device))
            throw new DeviceApiException(new ErrorMessage(Errors.DEVICE_NOT_FOUND));
    }

    public static void validateMacAddressExistInDatabase(List<DeviceData> deviceDataList) throws DeviceApiException {
        if (CollectionUtils.isNotEmpty(deviceDataList))
            throw new DeviceApiException(new ErrorMessage(Errors.MAC_ADDRESS_ALREADY_REGISTERED));
    }

    public static void validateUplinkMacAddressNotExistInDatabase(String uplinkMacAddress, List<DeviceData> deviceDataList) throws DeviceApiException {
        if (Objects.nonNull(uplinkMacAddress) && CollectionUtils.isEmpty(deviceDataList))
            throw new DeviceApiException(new ErrorMessage(Errors.UPLINK_MAC_ADDRESS_NOT_EXIST));
    }

}
