package io.restudie.validation;

import lombok.Data;

@Data
public class ErrorMessage implements Message {
    private Integer code;
    private String text;

    public ErrorMessage(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    public ErrorMessage(Errors error){
        this.code = error.getCode();
        this.text = error.getText();
    }
}

