package io.restudie.controller.mapper.impl;

import io.restudie.controller.response.AllDevicesResponse;
import io.restudie.controller.response.DeviceByMacAddressResponse;
import io.restudie.controller.mapper.MapToResponse;
import io.restudie.model.Device;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AllDevicesResponseMapperImpl implements MapToResponse<List<Device>, AllDevicesResponse> {

    private MapToResponse<Device, DeviceByMacAddressResponse> mapper =  new DeviceByMacAddressResponseMapperImpl();

    @Override
    public AllDevicesResponse toResponse(List<Device> devices) {
        AllDevicesResponse response = new AllDevicesResponse();
        for(Device device : devices){
            response.addDevice(mapper.toResponse(device));
        }

        return response;
    }
}
