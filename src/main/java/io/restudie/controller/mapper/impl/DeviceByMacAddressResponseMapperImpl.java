package io.restudie.controller.mapper.impl;

import io.restudie.controller.mapper.MapToResponse;
import io.restudie.model.Device;
import io.restudie.controller.response.DeviceByMacAddressResponse;
import org.springframework.stereotype.Component;

@Component
public class DeviceByMacAddressResponseMapperImpl implements MapToResponse<Device, DeviceByMacAddressResponse> {
    @Override
    public DeviceByMacAddressResponse toResponse(Device device) {
        DeviceByMacAddressResponse response = new DeviceByMacAddressResponse();

        response.setDeviceType(device.getType());
        response.setMacAddress(device.getMacAddress());

        return response;
    }
}
