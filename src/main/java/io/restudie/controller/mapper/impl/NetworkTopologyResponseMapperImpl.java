package io.restudie.controller.mapper.impl;

import io.restudie.controller.mapper.MapToResponse;
import io.restudie.controller.response.NetworkTopologyResponse;
import io.restudie.controller.response.NetworkTopologyResponseEntry;
import io.restudie.model.NetworkDevice;
import io.restudie.model.NetworkTopology;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;

@Component
public class NetworkTopologyResponseMapperImpl implements MapToResponse<NetworkTopology, NetworkTopologyResponse> {

    @Override
    public NetworkTopologyResponse toResponse(NetworkTopology networkTopology) {
        NetworkTopologyResponse response = new NetworkTopologyResponse();

        for (NetworkDevice networkDevice : networkTopology.values()) {
            response.addNetworkTopologyEntry(buildNetworkTopologyResponseEntries(networkDevice, new NetworkTopologyResponseEntry()));
        }

        return response;
    }


    private NetworkTopologyResponseEntry buildNetworkTopologyResponseEntries(NetworkDevice networkDevice, NetworkTopologyResponseEntry responseEntry) {
        responseEntry.setMacAddress(networkDevice.getMacAddress());
        if (CollectionUtils.isNotEmpty(networkDevice.getLinkedDevices())) {
            for (NetworkDevice linkedDevice : networkDevice.getLinkedDevices()) {
                responseEntry.addChildEntry(buildNetworkTopologyResponseEntries(linkedDevice, new NetworkTopologyResponseEntry()));
            }
        }
        return responseEntry;
    }
}
