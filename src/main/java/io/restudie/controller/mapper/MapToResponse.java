package io.restudie.controller.mapper;

public interface MapToResponse<D, R> {
    R toResponse(D d);
}
