package io.restudie.controller.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.restudie.validation.Message;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BasicResponse {
    private List<Message> validationMessages;
    private String technicalMessage;

    public BasicResponse() {

    }

    public BasicResponse(List<Message> validationMessages, String technicalMessage) {
        this.validationMessages = validationMessages;
        this.technicalMessage = technicalMessage;
    }

}
