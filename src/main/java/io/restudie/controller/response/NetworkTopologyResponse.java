package io.restudie.controller.response;

import lombok.Data;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Data
public class NetworkTopologyResponse extends BasicResponse{
    private List<NetworkTopologyResponseEntry> networkTopology;

    public void addNetworkTopologyEntry(NetworkTopologyResponseEntry entry){
        if(CollectionUtils.isEmpty(networkTopology)) this.networkTopology = new ArrayList<>();

        this.networkTopology.add(entry);
    }
}
