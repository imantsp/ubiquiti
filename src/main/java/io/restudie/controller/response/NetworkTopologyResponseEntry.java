package io.restudie.controller.response;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonFormat(shape = JsonFormat.Shape.ARRAY)
public class NetworkTopologyResponseEntry{
    private String macAddress;

    List<NetworkTopologyResponseEntry> linked = new ArrayList<>();

    public void addChildEntry(NetworkTopologyResponseEntry networkTopologyResponseEntry){
        this.linked.add(networkTopologyResponseEntry);
    }
}
