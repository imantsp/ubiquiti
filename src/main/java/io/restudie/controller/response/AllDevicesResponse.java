package io.restudie.controller.response;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class AllDevicesResponse extends BasicResponse{

    private List<DeviceByMacAddressResponse> devices;

    public void addDevice(DeviceByMacAddressResponse device){
        if (devices == null){
            devices = new ArrayList<>();
        }

        devices.add(device);
    }
}
