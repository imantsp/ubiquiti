package io.restudie.controller.response;

import io.restudie.model.enums.DeviceType;
import lombok.Data;

@Data
public class DeviceByMacAddressResponse extends BasicResponse{
    private DeviceType deviceType;
    private String macAddress;
}
