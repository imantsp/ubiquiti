package io.restudie.controller;

import io.restudie.controller.mapper.MapToResponse;
import io.restudie.exception.DeviceApiException;
import io.restudie.model.Device;
import io.restudie.model.NetworkTopology;
import io.restudie.model.enums.DeviceType;

import io.restudie.controller.response.AllDevicesResponse;
import io.restudie.controller.response.DeviceByMacAddressResponse;
import io.restudie.controller.response.NetworkTopologyResponse;
import io.restudie.service.DeviceApiService;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/device-management")
@Tag(name = "Device API controller")
public class DeviceApiController {
    private final DeviceApiService deviceApiService;
    private final MapToResponse<List<Device>, AllDevicesResponse> allDevicesResponseMapper;
    private final MapToResponse<Device, DeviceByMacAddressResponse> deviceByMacAddressResponseMapper;
    private final MapToResponse<NetworkTopology, NetworkTopologyResponse> networkTopologyResponseMapper;

    public DeviceApiController(DeviceApiService deviceApiService,
                               MapToResponse<List<Device>, AllDevicesResponse> allDevicesResponseMapper,
                               MapToResponse<Device, DeviceByMacAddressResponse> deviceByMacAddressResponseMapper,
                               MapToResponse<NetworkTopology, NetworkTopologyResponse> networkTopologyResponseMapper) {
        this.deviceApiService = Objects.requireNonNull(deviceApiService);
        this.allDevicesResponseMapper = Objects.requireNonNull(allDevicesResponseMapper);
        this.deviceByMacAddressResponseMapper = Objects.requireNonNull(deviceByMacAddressResponseMapper);
        this.networkTopologyResponseMapper = Objects.requireNonNull(networkTopologyResponseMapper);
    }

    @Operation(summary = "Register device in repository")
    @PostMapping("/device/register")
    public void registerDevice(@RequestParam DeviceType deviceType, @RequestParam String macAddress, @RequestParam(required = false) String uplinkMacAddress) throws DeviceApiException {
        deviceApiService.registerDevice(new Device(deviceType, macAddress, uplinkMacAddress));
    }

    @Operation(summary = "Retrieving all registered devices, sorted by device type")
    @GetMapping("/device/all")
    public AllDevicesResponse getAllDevices() throws DeviceApiException {
        return allDevicesResponseMapper.toResponse(deviceApiService.getDeviceList(true));
    }

    @Operation(summary = "Retrieving network deployment device by MAC address")
    @GetMapping("/device")
    public DeviceByMacAddressResponse getDeviceByMacAddress(@RequestParam String macAddress) throws DeviceApiException {
        return deviceByMacAddressResponseMapper.toResponse(deviceApiService.getDeviceByMacAddress(macAddress));
    }

    @Operation(summary = "Retrieving all registered network device topology")
    @GetMapping("/topology/all")
    public NetworkTopologyResponse getNetworkTopology() throws DeviceApiException {
        return networkTopologyResponseMapper.toResponse(deviceApiService.getNetworkTopology());
    }

    @Operation(summary = "Retrieving network device topology starting from a specific device")
    @GetMapping("/topology")
    public NetworkTopologyResponse getNetworkTopologyByMacAddress(@RequestParam String macAddress) throws DeviceApiException {
        return networkTopologyResponseMapper.toResponse(deviceApiService.getNetworkTopologyByMacAddress(macAddress));
    }
}
