package io.restudie.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Tag(name = "Controller for  application health services")
public class HealthController {
    @Operation(summary = "Health check of service")
    @GetMapping("/status")
    public String status() {
        return "{\"application\": \"device-management\",\"status\":\"UP\"}";
    }
}
