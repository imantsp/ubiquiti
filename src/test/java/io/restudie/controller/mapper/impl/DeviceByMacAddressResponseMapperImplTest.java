package io.restudie.controller.mapper.impl;

import io.restudie.controller.mapper.MapToResponse;
import io.restudie.controller.response.AllDevicesResponse;
import io.restudie.controller.response.DeviceByMacAddressResponse;
import io.restudie.model.Device;
import io.restudie.model.enums.DeviceType;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class DeviceByMacAddressResponseMapperImplTest {

    MapToResponse<Device, DeviceByMacAddressResponse> mapper = new DeviceByMacAddressResponseMapperImpl();

    @Test
    void toResponse() {

        //given
        Device device1 = new Device(DeviceType.ACCESSPOINT, "MAC1", null);

        //when
        DeviceByMacAddressResponse response1 = mapper.toResponse(device1);

        //then
        assertAll(
                () -> assertThat(response1).isNotNull(),
                () -> assertThat(response1.getMacAddress()).isNotBlank(),
                () -> assertThat(response1.getDeviceType()).isEqualTo(device1.getType()));
    }
}