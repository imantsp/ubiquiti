package io.restudie.controller.mapper.impl;

import io.restudie.controller.mapper.MapToResponse;
import io.restudie.controller.response.AllDevicesResponse;
import io.restudie.model.Device;
import io.restudie.model.enums.DeviceType;
import io.restudie.repository.entity.DeviceData;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class AllDevicesResponseMapperImplTest {

    MapToResponse<List<Device>, AllDevicesResponse> mapper = new AllDevicesResponseMapperImpl();

    @Test
    void toResponse() {

        //given
        Device device1 = new Device(DeviceType.ACCESSPOINT,"MAC1", null);
        Device device2 = new Device(DeviceType.GATEWAY,"MAC2", null);
        Device device3 = new Device(DeviceType.SWITCH,"MAC3", "MAC2");

        List<Device> deviceList = new ArrayList<>();
        deviceList.add(device1);
        deviceList.add(device2);
        deviceList.add(device3);

        //when
        AllDevicesResponse response = mapper.toResponse(deviceList);

        //then
        assertAll(
                () -> assertThat(response).isNotNull(),
                () -> assertThat(response.getDevices().size()).isEqualTo(3));
    }
}