package io.restudie.controller.mapper.impl;

import io.restudie.controller.mapper.MapToResponse;
import io.restudie.controller.response.DeviceByMacAddressResponse;
import io.restudie.controller.response.NetworkTopologyResponse;
import io.restudie.model.Device;
import io.restudie.model.NetworkDevice;
import io.restudie.model.NetworkTopology;
import io.restudie.model.enums.DeviceType;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class NetworkTopologyResponseMapperImplTest {

    MapToResponse<NetworkTopology, NetworkTopologyResponse> mapper = new NetworkTopologyResponseMapperImpl();

    @Test
    void toResponse() {

        //given
        Device device1 = new Device(DeviceType.ACCESSPOINT, "MAC1", null);
        Device device2 = new Device(DeviceType.GATEWAY, "MAC2", null);
        Device device3 = new Device(DeviceType.SWITCH, "MAC3", null);

        NetworkTopology networkTopology = new NetworkTopology();
        networkTopology.put(device1.getMacAddress(), new NetworkDevice(device1));
        networkTopology.put(device2.getMacAddress(), new NetworkDevice(device2));
        networkTopology.put(device3.getMacAddress(), new NetworkDevice(device3));

        //when
        NetworkTopologyResponse response = mapper.toResponse(networkTopology);

        //then
        assertAll(
                () -> assertThat(response).isNotNull(),
                () -> assertThat(response.getNetworkTopology()).size().isEqualTo(3));
    }
}