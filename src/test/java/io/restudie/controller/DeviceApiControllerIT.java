package io.restudie.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.restudie.controller.mapper.MapToResponse;
import io.restudie.controller.response.AllDevicesResponse;
import io.restudie.controller.response.DeviceByMacAddressResponse;
import io.restudie.controller.response.NetworkTopologyResponse;
import io.restudie.model.Device;
import io.restudie.model.NetworkTopology;
import io.restudie.model.enums.DeviceType;
import io.restudie.service.DeviceApiService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DeviceApiController.class)
@DisplayName("Accident controller")
class DeviceApiControllerIT {
    @MockBean
    private DeviceApiService deviceApiService;

    @MockBean
    private MapToResponse<List<Device>, AllDevicesResponse> allDevicesResponseMapper;

    @MockBean
    private MapToResponse<Device, DeviceByMacAddressResponse> deviceByMacAddressResponseMapper;

    @MockBean
    private MapToResponse<NetworkTopology, NetworkTopologyResponse> networkTopologyResponseMapper;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("Verifying registering of device")
    void registerDevice() throws Exception {
        mockMvc.perform(post("/device-management/device/register")
                        .param("deviceType", DeviceType.GATEWAY.name())
                        .param("macAddress", "MAC1")
                        .param("uplinkMacAddress", "MAC2")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk());
        then(deviceApiService).should().registerDevice(new Device(DeviceType.GATEWAY, "MAC1", "MAC2"));

        mockMvc.perform(post("/device-management/device/register")
                        .param("deviceType", DeviceType.GATEWAY.name())
                        .param("macAddress", "MAC1")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk());
        then(deviceApiService).should().registerDevice(new Device(DeviceType.GATEWAY, "MAC1", null));
    }

    @Test
    @DisplayName("Verifying to get all devices")
    void getAllDevices() throws Exception {
        mockMvc.perform(get("/device-management/device/all")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk());
        then(deviceApiService).should().getDeviceList(true);
    }

    @Test
    @DisplayName("Verifying to get device by mac address")
    void getDeviceByMacAddress() throws Exception {
        mockMvc.perform(get("/device-management/device")
                        .param("macAddress", "MAC1")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk());
        then(deviceApiService).should().getDeviceByMacAddress("MAC1");
    }

    @Test
    @DisplayName("Verifying to get network topology")
    void getNetworkTopology() throws Exception {
        mockMvc.perform(get("/device-management/topology/all")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk());
        then(deviceApiService).should().getNetworkTopology();
    }

    @Test
    @DisplayName("Verifying to get network topology starting from specified mac address")
    void getNetworkTopologyByMacAddress() throws Exception {
        mockMvc.perform(get("/device-management/topology")
                        .param("macAddress", "MAC1")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk());
        then(deviceApiService).should().getNetworkTopologyByMacAddress("MAC1");
    }
}