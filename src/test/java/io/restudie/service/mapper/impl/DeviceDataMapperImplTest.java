package io.restudie.service.mapper.impl;

import io.restudie.exception.DeviceApiException;
import io.restudie.model.Device;
import io.restudie.model.enums.DeviceType;
import io.restudie.repository.entity.DeviceData;
import io.restudie.repository.entity.DeviceTypeData;
import io.restudie.service.mapper.DeviceDataMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Device entity mapper DTO <-> Entity")
class DeviceDataMapperImplTest {

    DeviceDataMapper<Device, DeviceData> deviceDataMapper = new DeviceDataMapperImpl();

    @Test
    @DisplayName("When input data is not given, then null must be return")
    void toDto_test1() {
        //when
        Device device = deviceDataMapper.toDto(null);

        //then
        assertThat(device).isNull();
    }

    @Test
    @DisplayName("When valid data is given")
    void toDto_test2() {
        //given
        DeviceTypeData deviceTypeData = new DeviceTypeData();
        deviceTypeData.setId(3L);
        deviceTypeData.setName("ACCESSPOINT");
        DeviceData deviceData = new DeviceData();
        deviceData.setId(1L);
        deviceData.setType(deviceTypeData);
        deviceData.setMacAddress("MAC1");

        //when
        Device device = deviceDataMapper.toDto(deviceData);

        //then
        assertAll(
                () -> assertThat(device).isNotNull(),
                () -> assertThat(device.getMacAddress()).isEqualTo("MAC1"),
                () -> assertThat(device.getType()).isEqualTo(DeviceType.ACCESSPOINT));
    }

    @Test
    void toEntity() throws DeviceApiException {
        //given
        Device device = new Device(DeviceType.ACCESSPOINT, "MAC1", null);

        //when
        DeviceData deviceData = deviceDataMapper.toEntity(device);

        //then
        assertAll(
                () -> assertThat(deviceData).isNotNull(),
                () -> assertThat(deviceData.getMacAddress()).isEqualTo("MAC1"));
    }
}