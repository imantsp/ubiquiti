package io.restudie.service.mapper.impl;

import io.restudie.exception.DeviceApiException;
import io.restudie.model.Device;
import io.restudie.model.NetworkDevice;
import io.restudie.model.enums.DeviceType;
import io.restudie.repository.entity.DeviceData;
import io.restudie.repository.entity.DeviceTypeData;
import io.restudie.service.mapper.NetworkDeviceMapper;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.*;

class NetworkDeviceMapperImplTest {

    NetworkDeviceMapper<NetworkDevice, DeviceData> mapper = new NetworkDeviceMapperImpl();

    @Test
    void toDto() {
        //given
        DeviceTypeData deviceTypeData = new DeviceTypeData();
        deviceTypeData.setId(3L);
        deviceTypeData.setName("ACCESSPOINT");
        DeviceData deviceData = new DeviceData();
        deviceData.setId(1L);
        deviceData.setType(deviceTypeData);
        deviceData.setMacAddress("MAC1");

        //when
        NetworkDevice networkDevice = mapper.toDto(deviceData);

        //then
        assertAll(
                () -> assertThat(networkDevice).isNotNull(),
                () -> assertThat(networkDevice.getMacAddress()).isEqualTo(deviceData.getMacAddress()),
                () -> assertThat(networkDevice.getType()).isEqualTo(DeviceType.ACCESSPOINT));
    }

    @Test
    void toEntity() throws DeviceApiException{
        //given
        NetworkDevice networkDevice = new NetworkDevice(new Device(DeviceType.ACCESSPOINT,"MAC1", null));

        //when

        //then
        assertThatExceptionOfType(DeviceApiException.class).isThrownBy(()->{
            DeviceData deviceData = mapper.toEntity(networkDevice);
        });
    }
}