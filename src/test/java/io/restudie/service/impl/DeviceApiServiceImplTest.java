package io.restudie.service.impl;

import io.restudie.exception.DeviceApiException;
import io.restudie.model.Device;
import io.restudie.model.NetworkDevice;
import io.restudie.model.NetworkTopology;
import io.restudie.model.enums.DeviceType;
import io.restudie.repository.DeviceRepository;
import io.restudie.repository.DeviceTypeRepository;
import io.restudie.repository.entity.DeviceData;
import io.restudie.repository.entity.DeviceTypeData;
import io.restudie.service.DeviceApiService;
import io.restudie.service.mapper.DeviceDataMapper;
import io.restudie.validation.Errors;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.util.Optionals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
@DisplayName("Device Api Service")
class DeviceApiServiceImplTest {

    @Mock
    private DeviceRepository deviceRepository;

    @Mock
    private DeviceTypeRepository deviceTypeRepository;

    @Mock
    private DeviceDataMapper<Device, DeviceData> deviceDataMapper;

    @InjectMocks
    private DeviceApiServiceImpl deviceApiService;

    @Test
    @DisplayName("When MAC address doesn't exist")
    void registerDevice() throws DeviceApiException {
        //given
        DeviceTypeData deviceTypeData = new DeviceTypeData();
        deviceTypeData.setId(3L);
        deviceTypeData.setName("ACCESSPOINT");
        DeviceData deviceData = new DeviceData();
        deviceData.setId(1L);
        deviceData.setType(deviceTypeData);
        deviceData.setMacAddress("MAC1");
        given(deviceRepository.findDeviceDataByMacAddressContainingIgnoreCase("MAC1")).willReturn(new ArrayList<>());
        given(deviceTypeRepository.findById(deviceTypeData.getId())).willReturn(Optional.of(deviceTypeData));
        given(deviceDataMapper.toEntity(new Device(DeviceType.ACCESSPOINT, "MAC1", null))).willReturn(deviceData);

        //when
        deviceApiService.registerDevice(new Device(DeviceType.ACCESSPOINT, "MAC1", null));

        //then
        then(deviceRepository).should().save(deviceData);
    }

    @Test
    void getDeviceList() throws DeviceApiException {
        //given
        DeviceTypeData deviceTypeData = new DeviceTypeData();
        deviceTypeData.setId(3L);
        deviceTypeData.setName("ACCESSPOINT");
        DeviceTypeData deviceTypeData2 = new DeviceTypeData();
        deviceTypeData2.setId(2L);
        deviceTypeData2.setName("SWITCH");

        List<DeviceData> deviceDataList = new ArrayList<>();
        DeviceData deviceData = new DeviceData();
        deviceData.setType(deviceTypeData);
        deviceData.setMacAddress("MAC1");
        DeviceData deviceData2 = new DeviceData();
        deviceData2.setType(deviceTypeData2);
        deviceData2.setMacAddress("MAC2");

        deviceDataList.add(deviceData);
        deviceDataList.add(deviceData2);
        given(deviceRepository.findAll()).willReturn(deviceDataList);
        given(deviceDataMapper.toDto(deviceData)).willReturn(new Device(DeviceType.ACCESSPOINT, "MAC1", null));
        given(deviceDataMapper.toDto(deviceData2)).willReturn(new Device(DeviceType.SWITCH, "MAC2", null));

        //when
        List<Device> deviceList = deviceApiService.getDeviceList(true);

        //then
        assertAll(
                () -> assertThat(deviceList).isNotEmpty(),
                () -> assertThat(deviceList.stream().findFirst().get().getType().equals(DeviceType.SWITCH))
        );
    }

    @Test
    void getDeviceByMacAddress() throws DeviceApiException {
        //given
        DeviceTypeData deviceTypeData = new DeviceTypeData();
        deviceTypeData.setId(1L);
        deviceTypeData.setName("GATEWAY");

        List<DeviceData> deviceDataList = new ArrayList<>();
        DeviceData deviceData = new DeviceData();
        deviceData.setType(deviceTypeData);
        deviceData.setMacAddress("MAC1");
        deviceDataList.add(deviceData);
        given(deviceRepository.findDeviceDataByMacAddressContainingIgnoreCase("MAC1")).willReturn(deviceDataList);
        given(deviceDataMapper.toDto(deviceData)).willReturn(new Device(DeviceType.GATEWAY, "MAC1", null));

        //when
        Device device = deviceApiService.getDeviceByMacAddress("MAC1");

        //then
        assertAll(
                () -> assertThat(device).isNotNull(),
                () -> assertThat(device.getMacAddress().equals("MAC1"))
        );
    }

    @Test
    void getNetworkTopology() throws DeviceApiException {
        //given
        Device device = new Device(DeviceType.GATEWAY, "MAC1", null);

        DeviceTypeData deviceTypeData = new DeviceTypeData();
        deviceTypeData.setId(1L);
        deviceTypeData.setName("GATEWAY");

        List<DeviceData> deviceDataList = new ArrayList<>();
        DeviceData deviceData = new DeviceData();
        deviceData.setType(deviceTypeData);
        deviceData.setMacAddress("MAC1");
        deviceDataList.add(deviceData);
        given(deviceDataMapper.toDto(deviceData)).willReturn(device);
        given(deviceRepository.findAll()).willReturn(deviceDataList);

        //when
        NetworkTopology networkTopology = deviceApiService.getNetworkTopology();

        //then
        assertAll(
                () -> assertThat(networkTopology).isNotEmpty(),
                () -> assertThat(networkTopology.get("MAC1").getMacAddress().equals("MAC1"))
        );
    }

    @Test
    @DisplayName("When MAC address doesn't exist")
    void getNetworkTopologyByMacAddress_test1() {
        //when
        DeviceApiException exception = assertThrows(DeviceApiException.class, () -> deviceApiService.getNetworkTopologyByMacAddress("MAC1"));

        //then
        assertAll(
                () -> assertThat(exception).isNotNull(),
                () -> assertThat(exception.getErrorMessages().contains(Errors.DEVICE_NOT_FOUND)),
                () -> assertThat(exception.getMessage()).isNull()
        );
    }

    @Test
    @DisplayName("When MAC address exist")
    void getNetworkTopologyByMacAddress_test2() throws DeviceApiException {
        //given
        Device device = new Device(DeviceType.GATEWAY, "MAC1", null);

        DeviceTypeData deviceTypeData = new DeviceTypeData();
        deviceTypeData.setId(1L);
        deviceTypeData.setName("GATEWAY");

        List<DeviceData> deviceDataList = new ArrayList<>();
        DeviceData deviceData = new DeviceData();
        deviceData.setType(deviceTypeData);
        deviceData.setMacAddress("MAC1");
        deviceDataList.add(deviceData);
        given(deviceRepository.findDeviceDataByMacAddressContainingIgnoreCase("MAC1")).willReturn(deviceDataList);
        given(deviceDataMapper.toDto(deviceData)).willReturn(device);
        given(deviceRepository.findAll()).willReturn(deviceDataList);

        //when
        NetworkTopology networkTopology = deviceApiService.getNetworkTopologyByMacAddress("MAC1");

        //then
        assertAll(
                () -> assertThat(networkTopology).isNotEmpty(),
                () -> assertThat(networkTopology.get("MAC1").getMacAddress().equals("MAC1"))
        );
    }
}